var app = angular.module('HomeApp', ['ngRoute', 'ui.bootstrap', 'LocalStorageModule', 'ngLocale']);


/*
* Makes the user able to change 'page'
 */

app.config(['$routeProvider',function($routeProvider) {
    $routeProvider
        .when("/", {
            activetab : "today"
        })
        .when("/week", {
            activetab : "week"
        })
        .when("/month", {
            activetab : "month"
        })
        .when("/all", {
            activetab : "all"
        });

}]);


/*
* Runs the fillAuthData from authService, every time someone enters the website.
* This is to check, if a user is saved on the localStorage of the computer.
 */
app.run(['authService', function (authService) {
    authService.fillAuthData();
}]);


/*
* Configures the app, so http requests happens in a certain way.
* Solves caching problems in IE11.
*
* $httpProvider.interceptors - Every time a http request is made, the request method is called in the authInterceptorService.
 */
app.config(function ($httpProvider) {
    if (!$httpProvider.defaults.headers.get) {
        $httpProvider.defaults.headers.get = {};
    }

    // Answer edited to include suggestions from comments
    // because previous version of code introduced browser-related errors

    //disable IE ajax request caching
    $httpProvider.defaults.headers.get['If-Modified-Since'] = 'Mon, 26 Jul 1997 05:00:00 GMT';

    $httpProvider.interceptors.push('authInterceptorService');

});






