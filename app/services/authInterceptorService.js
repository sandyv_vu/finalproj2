app.factory('authInterceptorService', ['$q', '$location', 'localStorageService', function ($q, $location, localStorageService) {


    var authInterceptorServiceFactory = {};


    /*
    *
    * Instead of adding the user's token, in each http request,
    * this function, adds the token every time a http request is made.
     */
    var _request = function (config) {

        config.headers = config.headers || {};

        var authData = localStorageService.get('authorizationData');    
        if (authData != undefined) {
            config.headers.Authorization = 'Bearer ' + authData.token;
        }

        return config;
    }


    /*
    * When the user is not able to make a http request,
    * a rejection is made.
     */
    var _responseError = function (rejection) {
        if (rejection.status === 401) {
            $location.path('');
        }
        return $q.reject(rejection);
    }


    authInterceptorServiceFactory.request = _request;
    authInterceptorServiceFactory.responseError = _responseError;

    return authInterceptorServiceFactory;
}]);