app.factory('authService', ['$http', '$q', 'localStorageService', '$rootScope', function ($http, $q, localStorageService, $rootScope) {

    var serviceBase = 'http://dw9project.azurewebsites.net/';
    // var serviceBase = "http://localhost:9736/";

    var authServiceFactory = {};

    var _authentication = {
        isAuth: false,
        userName : ""
    };


    /*
    * This method sets the user information in a localStorage, so every time the user goes to the website,
    * the user is still logged in. This expires after 24 hours, according to the OAuth settings in the backend.
     */
    var _login = function (loginData) {
            var data = "grant_type=password&username=" + loginData.userName + "&password=" + loginData.password;

            var deferred = $q.defer();
            $http.post(serviceBase + 'token', data, {headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'}}).then(function (response) {

                localStorageService.set('authorizationData', {
                    token: response.data.access_token,
                    userName: loginData.userName,
                    userId: response.data.UserId
                });
                _authentication.isAuth = true;
                _authentication.userName = loginData.userName;

                deferred.resolve(response);
            });

            return deferred.promise;
    };

    /*
    * Makes the user able to log out manually, instead of waiting for the token to expire.
     */
    var _logOut = function () {

        localStorageService.remove('authorizationData');

        _authentication.isAuth = false;
        _authentication.userName = "";

    };


    /*
    * Every time a user goes to the website, this method is called,
    * to check whether there is a user saved in the localStorage.
    * If this is the case, the user is automatically logged in.
     */
    var _fillAuthData = function () {

        var authData = localStorageService.get('authorizationData');
        if (authData)
        {
            _authentication.isAuth = true;
            _authentication.userName = authData.userName;
            $rootScope.UserID = authData.userId;

        }
    };

    authServiceFactory.login = _login;
    authServiceFactory.logOut = _logOut;
    authServiceFactory.fillAuthData = _fillAuthData;
    authServiceFactory.authentication = _authentication;

    return authServiceFactory;
}]);
