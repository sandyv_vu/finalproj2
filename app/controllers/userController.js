app.controller('userController', ['$scope', '$location', 'authService', '$http', '$rootScope', function ($scope, $location, authService, $http, $rootScope) {

    var url = "http://dw9project.azurewebsites.net/";
    // var url = "http://localhost:9736/";

    $scope.createUsername = null;
    $scope.createPass = null;
    $scope.login = null;
    $scope.message = "";


    /*
    * The users login data.
     */
    $scope.loginData = {
        userName: "",
        password: ""
    };


    /*
    * Makes it possible for a user to login.
    * This function, calls another function, which provides the user with a token,
    * so the user is able to make calls to the API, which is protected with OAuth.
    *
    * $rootScope.$emit("Login", {}) - Emits the 'login' so the rootScope which listens to Login gets called.
     */
    $scope.login = function () {
        if($scope.loginData.userName != undefined && $scope.loginData.password != undefined) {
            authService.login($scope.loginData).then(function (response) {
                    $location.path("");
                    $rootScope.$emit("Login", {});
                    $rootScope.UserID = response.data.UserId;
                },
                function errorHandling(err) {
                    $scope.message = err.error_description;
                });
        }
    };


    /*
     * Makes it possible for a user to log off.
     * This function, calls another function, which provides removes the user from the localStorage,
     *
     * $rootScope.$emit("LogOut", {}) - Emits the 'LogOut' so the rootScope which listens to 'LogOut' gets called.
     */
    $scope.logOut = function () {
        authService.logOut();
        $rootScope.$emit("LogOut", {});
        $location.path('');
    };


    /*
    * Creates a user, whom gets logged on immediately when signed up.
    *
     */
    $scope.createUser = function () {
        if($scope.loginData.userName != undefined && $scope.loginData.password != undefined) {
            var data = {username: $scope.loginData.userName, password: $scope.loginData.password};
            $http.post(url + "users/signup/", data).then(function (response) {
                $scope.login();
            }, function errorHandling (error) {
                $scope.signupError = "Username taken"
            });
        }
    };
    $scope.authentication = authService.authentication;
}]);