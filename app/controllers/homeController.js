app.controller('homeController', function($scope,$http, $route, $filter, $rootScope, authService, $q) {

        var url = "http://dw9project.azurewebsites.net/";
        // var url = "http://localhost:9736/";

        var startTime = new Date().setHours(new Date().getHours()+1,0,0,0);
        var endTime = new Date().setHours(new Date().getHours()+2,0,0,0);

        $scope.reminderStart = new Date(startTime);
        $scope.reminderEnd = new Date(endTime);
        $scope.$route = $route;

        /*
        * Checks whether a user is logged in or not.
         */
        $scope.auth = false;


        /*
        /*
         * Keeps track of which tab the user is on.
         */
        $scope.tab = 0;


        /*
         * The whole list, which consist ALL of the reminders, the user has.
         * Doesn't get manipulated with.
         */
        $scope.remindersList = [];


        /*
         * The list, which gets manipulated with, ex. when the user sorts after reminders of the day,
         * this reminders list changes.
         */
        $scope.reminders = [];


        /*
         * Checks if a date equals another date
         */
        Date.prototype.isValid = function () {
            return this.getTime() === this.getTime();
        };

        /*
         * Checks which weeknumber it is
         *
         */
        Date.prototype.getWeek = function() {
            var d = new Date(Date.UTC(this.getFullYear(), this.getMonth(), this.getDate()));
            var dayNum = d.getUTCDay() || 7;
            d.setUTCDate(d.getUTCDate() + 4 - dayNum);
            var yearStart = new Date(Date.UTC(d.getUTCFullYear(),0,1));
            return Math.ceil((((d - yearStart) / 86400000) + 1)/7)
        };

        /*
         * Sorts the start time of a reminder
         * @param remindersList - Takes a reminderslist to sort.
         */
        var sort = function (remindersList) {
            remindersList.sort(function (a, b) {
                var dateA = new Date(a.StartTime).getTime();
                var dateB = new Date(b.StartTime).getTime();
                return dateA - dateB;
            });
        };


        /*
         * Checks whether the input provided is valid.
         * @param title - takes the title of a reminder
         * @param startTime - takes the start time of a reminder
         * @param endTime - takes the end time of a reminder
         */
        var validateInput = function(title, startTime, endTime) {
            if (title === undefined || title.length == 0) {
                $scope.lblError = "Title field is required";
                return false;
            }
            if (!startTime.isValid() || startTime <= Date.now()) {
                $scope.lblError = "Start date is not valid";
                return false;
            }
            if (!endTime.isValid() || endTime <= startTime) {
                $scope.lblError = "End date is not valid";
                return false;
            }
            $scope.lblError = "";
            return true;
        };


        /*
         * Formats a date to the right format
         */
        var formatDate = function(dateTime) {
            var date = dateTime.replace('T', ' ');
            var string = date + "";
            var a=string.split(" ");
            var d=a[0].split("-");
            var t=d[2].split("T");
            var k=a[1].split(":");

            return new Date(d[0],(d[1]-1),t[0], k[0], k[1]);
        };


        /*
         * This function filters so there's only 7 reminders, in each page.
         */
        $scope.filter = function () {
            $scope.filteredReminders = $scope.reminders;
            $scope.totalItems = $scope.filteredReminders.length;
            $scope.currentPage = 1;
            $scope.itemsPerPage = 7;

            $scope.$watch("currentPage", function() {
                setPagingData($scope.currentPage);
            });

            function setPagingData(page) {
                var pagedData = $scope.filteredReminders.slice(
                    (page - 1) * $scope.itemsPerPage,
                    page * $scope.itemsPerPage
                );
                $scope.reminders = pagedData;
            }


        };


        /*
         * Saves a reminders data, if clicked on.
         */
        $scope.setValue = function(list) {
            $scope.template = list;
            $scope._id = $scope.template.Id;
            $scope.title = $scope.template.Title;
            $scope.description = $scope.template.Description;
            $scope.place = $scope.template.Place;
            $scope.areaID = $scope.template.AreaID;

            var startTime = new Date($scope.template.StartTime);
            $scope.reminderStart = startTime;

            var endTime = new Date($scope.template.EndTime);
            $scope.reminderEnd = endTime;
        };


        /*
         * Clears values from input fields. ex. when a post is made.
         */
        $scope.clearValues = function() {
            $scope.title = "";
            $scope.place = "";
            $scope.description = "";
            var startTime = new Date().setHours(new Date().getHours()+1,0,0,0);
            var endTime = new Date().setHours(new Date().getHours()+2,0,0,0);
            $scope.reminderStart = new Date(startTime);
            $scope.reminderEnd = new Date(endTime);
            $scope.template = undefined;
            $scope._id = undefined;
            $scope.areaID = undefined;
        };


        /*
         * Saves reminder for when checkbox is clicked.
         */
        $scope.saveValue = function (item) {
            $scope.template = item;
        };


        /*
         * Gets all the reminders of the user logged in.
         */
        $scope.getReminders = function () {
            $http.get(url + "api/areas/items/Reminder").then(function (response) {
                var list = [];
                for(var i in response.data) {
                    if(response.data[i].Checked == false && response.data[i].UserID == $rootScope.UserID) {
                        list.push(response.data[i]);
                    }
                }
                $scope.remindersList = list;
                $scope.checkTab();
                $scope.auth = true;
            }, function errorHandling(error) {
            });
        };


        /*
         * When a user is logged in, this function is called,
         * so the user is able to see hers/his reminders.
         */
        $scope.getReminders();
        $rootScope.$on("Login", function(){
            $scope.getReminders();
        });


        /*
         * Removes the users reminders, when the user logs off.
         */
        $rootScope.$on('LogOut', function(){
            $scope.remindersList = [];
            $scope.reminders = [];
            $scope.auth = false;
            // canceler.resolve();
        });


        /*
         * Checks which tab the user is has clicked on.
         */
        $scope.checkTab = function () {
            if($scope.$route.current.activetab == "today") {
                $scope.getDailyReminders();
            } else if($scope.$route.current.activetab == "week"){
                $scope.getWeeklyReminders();
            } else if($scope.$route.current.activetab == "month"){
                $scope.getMonthlyReminders();
            } else if($scope.$route.current.activetab == "all") {
                $scope.getAllReminders();
            }
        };


        /*
         * Gets all the reminders for today
         */
        $scope.getDailyReminders = function () {
            $scope.tab = 1;
            $scope.dailyReminders = [];

            for(var i in $scope.remindersList){
                var startDate = formatDate($scope.remindersList[i].StartTime);
                var newDate = new Date();

                var startDay = new Date(startDate.getFullYear(), startDate.getMonth(), startDate.getDate());
                var newDay = new Date(newDate.getFullYear(), newDate.getMonth(), newDate.getDate());

                if (newDate <= startDate && startDay.getTime() == newDay.getTime()) {
                    $scope.dailyReminders.push($scope.remindersList[i]);
                }
            }

            sort($scope.dailyReminders);

            $scope.reminders = $scope.dailyReminders;

            $scope.filter();
        };


        /*
         * Gets all the weekly reminders
         */
        $scope.getWeeklyReminders = function () {
            $scope.tab = 2;
            $scope.weeklyReminders = [];

            for(var i in $scope.remindersList){
                var startDate = formatDate($scope.remindersList[i].StartTime);
                var newDate = new Date();
                if (startDate.getWeek() == newDate.getWeek() && startDate >= newDate) {
                    $scope.weeklyReminders.push($scope.remindersList[i]);
                }
            }

            sort($scope.weeklyReminders);

            $scope.reminders = $scope.weeklyReminders;

            $scope.filter();
        };


        /*
         * Gets all the monthly reminders
         */
        $scope.getMonthlyReminders = function () {
            $scope.tab = 3;
            $scope.monthlyReminders = [];

            for(var i in $scope.remindersList){
                var startDate = formatDate($scope.remindersList[i].StartTime);
                var newDate = new Date();
                if (startDate.getMonth() == newDate.getMonth() && startDate >= newDate) {
                    $scope.monthlyReminders.push($scope.remindersList[i]);
                }
            }

            sort($scope.monthlyReminders);

            $scope.reminders = $scope.monthlyReminders;

            $scope.filter();
        };


        /*
         * Shows all the reminders, the user has ever made.
         */
        $scope.getAllReminders = function () {
            $scope.tab = 4;
            sort($scope.remindersList);
            $scope.reminders = $scope.remindersList;
            $scope.filter();
        };


        /*
         * Adds a reminder to their list of reminders.
         */
        $scope.addReminders = function () {
            var data = {};
            var areaId = 0;

            var start = new Date($scope.reminderStart);
            var end = new Date($scope.reminderEnd);


            if (validateInput($scope.title, start, end)) {
                $http.post(url + "api/areas/", data).then(function (response) {
                    areaId = response.data;

                    var startTime = $filter('date')(start, 'yyyy-MM-dd HH:mm');
                    var endTime = $filter('date')(end, 'yyyy-MM-dd HH:mm');

                    var data = {
                        SystemName: "Reminder",
                        Input: {
                            Title: $scope.title,
                            Place: $scope.place,
                            Description: $scope.description,
                            StartTime: startTime,
                            EndTime: endTime,
                            Checked: false,
                            AreaID: areaId,
                            UserID: $rootScope.UserID
                        }
                    };
                    $http.post(url + "api/areas/items/" + areaId, data).then(function (response) {
                        $scope.title = undefined;
                        var a = response.data.StartTime.split(" ");
                        var d = a[0].split("-");
                        var t = a[1].split(":");

                        var createdAt = new Date(d[0], (d[1] - 1), d[2]);
                        response.data.StartTime = response.data.StartTime.split(' ').join('T');
                        response.data.EndTime = response.data.EndTime.split(' ').join('T');
                        $scope.remindersList.push(response.data);
                        var newDate = new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate());
                        if ($scope.tab == 1 && createdAt.getTime() == newDate.getTime()) {
                            $scope.getDailyReminders();
                        } else if ($scope.tab == 2 && createdAt.getWeek() == new Date().getWeek()) {
                            $scope.getWeeklyReminders();
                        } else if ($scope.tab == 3 && createdAt.getMonth() == new Date().getMonth()) {
                            $scope.getMonthlyReminders();
                        } else if ($scope.tab == 4) {
                            $scope.getAllReminders();
                        }

                        $scope.clearValues();
                    });
                });
            }

        };


        /*
         * Removes a reminder, from their list of reminders.
         */
        $scope.removeFromList = function () {
            if($scope.areaID !== undefined) {
                $http.delete(url + "api/areas/" + $scope.areaID).then(function (response) {
                    var i = $scope.reminders.indexOf($scope.template);
                    var j = $scope.remindersList.indexOf($scope.template);
                    $scope.remindersList.splice(j, 1);
                    $scope.reminders.splice(i, 1);
                    $scope.clearValues();
                    $scope.checkTab();
                });
                $scope.lblError = "";
            }
            else {
                $scope.lblError = "No reminder selected";
            }
        };


        /*
         * Edits a chosen reminder.
         */
        $scope.editReminder = function () {
            var start = new Date($scope.reminderStart);
            var end = new Date($scope.reminderEnd);

            if (($scope._id) === undefined) {
                $scope.lblError = "No reminder selected";
                return;
            }
            if (validateInput($scope.title, start, end)) {
                var startTime = $filter('date')(start, 'yyyy-MM-dd HH:mm');
                var endTime = $filter('date')(end, 'yyyy-MM-dd HH:mm');
                var data = {
                    Title: $scope.title,
                    Place: $scope.place,
                    Description: $scope.description,
                    StartTime: startTime,
                    EndTime: endTime
                };
                $http.put(url + "api/items/Reminder/" + $scope._id, data).then(function (response) {
                    var i = $scope.reminders.indexOf($scope.template);
                    var j = $scope.remindersList.indexOf($scope.template);
                    $scope.reminders[i].Title = $scope.title;
                    $scope.reminders[i].Place = $scope.place;

                    $scope.reminders[i].StartTime = startTime;

                    $scope.remindersList[j].Title = $scope.title;
                    $scope.remindersList[j].Place = $scope.place;
                    $scope.remindersList[j].Description = $scope.description;
                    $scope.remindersList[j].StartTime = startTime.replace(' ', 'T');
                    $scope.remindersList[j].EndTime = endTime.replace(' ', 'T');

                    $scope.clearValues();
                    $scope.checkTab();
                });
            }
        };


        /*
         * Dismiss a reminder. Ex. when the reminder is successfully done, the user can choose to dismiss it.
         */
        $scope.dismissReminder = function (item) {
            var data = {Checked: 1};
            $http.put(url + "api/items/Reminder/" + item.Id, data)
                .then(function (response) {


                    for (var i in $scope.remindersList) {
                        if ($scope.remindersList[i].Checked == true) {
                            $scope.remindersList.splice(i, 1);
                        }
                    }
                    var i = $scope.reminders.indexOf($scope.template);
                    $scope.clearValues();
                    $scope.checkTab();
                });
        };
    }
);